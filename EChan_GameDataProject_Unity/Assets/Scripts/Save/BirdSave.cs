﻿using System;
using UnityEngine;

//Class can be saved
[Serializable]

//Inherit from Save
public class BirdSave : Save
{

    public Data data;
    private Bird bird;
    private Animator animator;
    private AnimatorStateInfo animatorStateInfo;
    private string jsonString;

    //Class can be saved
    [Serializable]
    //Inherit from BaseData
    public class Data : BaseData
    {
        //Save the following 4 variables
        public Vector3 birdPosition;
        public int dirIndex;
        public float playbackPos;
        public float birdSpeed;

    }

    //Before game start, get reference to the bird script and animator component
    //Create new instance of data class
    void Awake()
    {
        bird = GetComponent<Bird>();
        animator = GetComponent<Animator>();
        data = new Data();

    }
    //On Save...
    public override string Serialize()
    {
        //Save the prefab name
        data.prefabName = prefabName;
        //Save this bird's position
        data.birdPosition = bird.transform.position;
        //Save which direction they were facing and flying in
        data.dirIndex = bird.directionIndex;
        //Get reference to info about the the animator's current state
        animatorStateInfo = animator.GetCurrentAnimatorStateInfo(0);
        //Save the current point in the playback time (so the animation continues where it left off)
        float playbackTime = animatorStateInfo.normalizedTime;
        data.playbackPos = playbackTime;
        //Save this bird's given speed
        data.birdSpeed = bird.speed;
        //Format to Json and save as a string
        jsonString = JsonUtility.ToJson(data);
        //Return this string
        return (jsonString);
    }

    //On Load...
    public override void Deserialize(string jsonData)
    {
        //Load the given Json data
        JsonUtility.FromJsonOverwrite(jsonData, data);
        //Load the bird's position
        bird.transform.position = data.birdPosition;
        //Load the direction the bird was facing
        bird.directionIndex = data.dirIndex;
        //Restart the animator to play at the given point in playback time
        animator.Play("BirdFly", 0, data.playbackPos);
        //Load the given speed
        bird.speed = data.birdSpeed;
        //Update the game object's name to Bird
        bird.name = "Bird";
    }
}
