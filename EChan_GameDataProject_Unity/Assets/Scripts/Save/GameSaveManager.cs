﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using System;
using System.IO;

[Serializable]
public class GameSaveManager : MonoBehaviour
{

    public string gameDataFilename = "game-save.json";
    private string gameDataFilePath;
    public List<string> saveItems;
    public bool firstPlay = true;

    // Singleton pattern from:
    // http://clearcutgames.net/home/?p=437

    // Static singleton property
    public static GameSaveManager Instance { get; private set; }



    //  to account for deprecated OnLevelWasLoaded() - http://answers.unity3d.com/questions/1174255/since-onlevelwasloaded-is-deprecated-in-540b15-wha.html
    void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }


    void Awake()
    {
        // First we check if there are any other instances conflicting
        if (Instance != null && Instance != this)
        {
            // If that is the case, we destroy other instances
            Destroy(gameObject);
            return;
        }


        // Here we save our singleton instance
        Instance = this;

        // Furthermore we make sure that we don't destroy between scenes (this is optional)
        DontDestroyOnLoad(gameObject);

        //Application.persistentDataPath returns a string path where data is saved outside of the project and on the computer
        //Add gameDataFilename to the end, the name of the json file game data is saved as
        //Combined String is saved as gameDataFilePath
        gameDataFilePath = Application.persistentDataPath + "/" + gameDataFilename;
        //Create new instance of the list, saveItems
        saveItems = new List<string>();
    }

    //When called, add the given item to the list of strings, saveItems
    public void AddObject(string item)
    {
        saveItems.Add(item);
    }


    //Save data pertaining to the tank, bullets, and turrets
    public void Save()
    {
        //clear the list, saveItems, of elements
        saveItems.Clear();

        //Create an array that finds all objects currently in the scene that are derived from the Save class
        //In this game, it will find any instances of the TankSave, TurretSave, and BulletSave scripts that exist in the scene
        Save[] saveableObjects = FindObjectsOfType(typeof(Save)) as Save[];

        //For each of these scripts...
        foreach (Save saveableObject in saveableObjects)
        {
            //Call the Serialize method to convert the data specified in the script into a Json string
            //Add the Json string for the object in the list of items to save
            saveItems.Add(saveableObject.Serialize());
        }

        //StreamWriter allows the script to write characters into the given file
        //Here, a StreamWriter is created so the script can write in the Json file that will hold the game save
        using (StreamWriter gameDataFileStream = new StreamWriter(gameDataFilePath))
        {
            //For each Json formatted string in the list...
            foreach (string item in saveItems)
            {
                //Write the string on a new line in the Json file
                gameDataFileStream.WriteLine(item);
            }
        }
    }

    //Reload the scene
    public void Load()
    {
        //By reloading, this scene is no longer the first time it has been loaded
        firstPlay = false;
        //Clear the list of saved items
        saveItems.Clear();
        //Call and load this scene
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }






    //Call this function when an event is called after the scene loads
    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        //If this is the first time the scene has been loaded, return at this point
        if (firstPlay) return;

        //Load the data from the Json file
        LoadSaveGameData();
        //Destroy all objects in the scene that can be saved
        DestroyAllSaveableObjectsInScene();
        //Recreate the saved objects using the loaded data
        CreateGameObjects();
    }

    void LoadSaveGameData()
    {
        //Create a StreamReader that allows the script to read the given file
        //Here, the StreamReader will read the Json file that holds the game save
        using (StreamReader gameDataFileStream = new StreamReader(gameDataFilePath))
        {
            //StreamReader.Peek reads through the file and returns an integer that represents the next character or if there are no characters to read (reached end of file)
            //While there are still characters to read in the file
            while (gameDataFileStream.Peek() >= 0)
            {
                //ReadLine reads each line of characters
                //Trim removes any blank spaces in the line read
                //Read each line and save it in a string
                string line = gameDataFileStream.ReadLine().Trim();
                //If the string is not empty
                if (line.Length > 0)
                {
                    //Add the string to the list of items saved
                    saveItems.Add(line);
                }
            }
        }
    }

    void DestroyAllSaveableObjectsInScene()
    {
        //Find all instances of scripts derived from the Save class in the scene
        //Save in an array
        Save[] saveableObjects = FindObjectsOfType(typeof(Save)) as Save[];
        //For each element in the array...
        foreach (Save saveableObject in saveableObjects)
        {
            //Destroy the game object
            Destroy(saveableObject.gameObject);
        }
    }


    void CreateGameObjects()
    {
        //for each string in the list
        foreach (string saveItem in saveItems)
        {
            //Create a string to represent the format for the name header
            //Placing an @ at the beginning so everything in the string is taken literally
            //To close it, it requires double double quotes "" something ""
            //Here, the actual string is "prefabName"":"
            string pattern = @"""prefabName"":""";
       
           //String.IndexOf finds the first time the string given occurs and returns the integer index of where it starts
           //Indexing starts at 0
           //Find where "prefabName"":" starts in the string
            int patternIndex = saveItem.IndexOf(pattern);
            //Find where the next ' " ' is in the string after the name header
            //Add 1 to the index to find the index of where the prefab name actually starts 
            int valueStartIndex = saveItem.IndexOf('"', patternIndex + pattern.Length - 1) + 1;
            //Find where the next ' " ' is after where the name starts
            int valueEndIndex = saveItem.IndexOf('"', valueStartIndex);
            //String.Substring gets a string within the given string starting at the given position and runs for the given length
            //Here, it is given where the prefab name starts, length is found by subtracting the index for the end from the index for where the name starts
            string prefabName = saveItem.Substring(valueStartIndex, valueEndIndex - valueStartIndex);
            //Create a new game object based on a prefab with the given prefab name
            GameObject item = Instantiate(Resources.Load(prefabName)) as GameObject;
            //Call the Deserialize function and pass the Json string for the object to load saved data into the object
            item.SendMessage("Deserialize", saveItem);
        }
    }
}
