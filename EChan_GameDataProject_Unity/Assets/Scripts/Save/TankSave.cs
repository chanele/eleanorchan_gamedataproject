﻿using System;
using UnityEngine;

//Serializable means the class can be converted into a stream that saves this instance of the class and stores it somewhere else, such as a file
//Allows the JsonUtility to modify the class
[Serializable]

//Save is an abstract class that cannot be instantiated itself, other classes must inherit from it (similar to interfaces, but only one abstract can be inherited from at a time)
//Are either partially implemented or not implemented at all
//Methods in an abstract class must be implemented by the inheriting class
//Abstract methods must be implemented using 'override' and have the same conditions and return values specified in the abstract method
//Because TankSave is inheriting from Save, it must override and implement Serialize and Deserialize
//It also receives the string variable prefabName
public class TankSave : Save {

    public Data data;
    private Tank tank;
    private string jsonString;

    //This class can be saved
    [Serializable]
    //Another class named Data is created under the TankSave class
    //Data inherits from the class, BaseData, receiving the string variable for prefabName
    public class Data : BaseData {
        //3 vector3 variables are created in the Data class
        //This is where the tank's position, rotation, and intended destination are saved
        public Vector3 position;
        public Vector3 eulerAngles;
        public Vector3 destination;
    }

    //Before the game starts, get reference to the Tank script on the same gameobject as this script
    //Create a new instance of the data class
    void Awake() {
        tank = GetComponent<Tank>();
        data = new Data();
    }

    //Override the abstract method from Save
    //When called, create a Json formatted string that includes the following information...
    public override string Serialize() {
        //Save the given prefabName in Tanksave in the data class
        data.prefabName = prefabName;
        //Save the tank's current position in the data class
        data.position = tank.transform.position;
        //Save the tank's current rotation in the data class (as a vector3, not quaternion)
        data.eulerAngles = tank.transform.eulerAngles;
        //Save the tank's intended destination in the data class
        data.destination = tank.destination;
        //JsonUtility.ToJson converts the given item into a format for Json
        //Passing data into the method means all the public fields in data (prefab name, tank's position, rotation, and destination) will be converted into Json
        //The Json format is saved as a string
        jsonString = JsonUtility.ToJson(data);
        //return the Json string as a result from this method
        return (jsonString);
    }

    //Override the abstract method from Save
    //When called, take the given Json formatted string and load the data from it
    public override void Deserialize(string jsonData) {
        //JsonUtility.FromJsonOverwrite takes the given Json formatted string and loads the data saved in it into the given object
        //Here it takes the Json string that was serialized from a previous instance of the data class and overwrites all the public fields in this instance of the data class with the previous values
        JsonUtility.FromJsonOverwrite(jsonData, data);
        //After loading the previous values into data, update the current instance of the tank with these values
        //Update tank position with position in data
        tank.transform.position = data.position;
        //Update tank rotation with rotation in data
        tank.transform.eulerAngles = data.eulerAngles;
        //Update tank's intended destination with destination in data
        tank.destination = data.destination;
        //Update game object's name to Tank
        tank.name = "Tank";
    }
}