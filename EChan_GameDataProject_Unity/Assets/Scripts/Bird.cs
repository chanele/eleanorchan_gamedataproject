﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bird : MonoBehaviour
{

    public float speed;
    public int directionIndex;
    public float setX, setY;
    public float startTime;
    public Transform topLeft, topRight, bottomRight, bottomLeft;
    private Rigidbody2D rigidBody;
    private Animator animator;

    // Use this for initialization
    void Awake()
    {
        //Get rigid body and spawning boundaries
        rigidBody = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        topLeft = GameObject.Find("Top Left").transform;
        topRight = GameObject.Find("Top Right").transform;
        bottomRight = GameObject.Find("Bottom Left").transform;
        bottomLeft = GameObject.Find("Bottom Right").transform;
        //Sets where animation starts, only needed for first load to desync animation between the birds
        animator.Play("BirdFly", 0, startTime);

    }

    void Update()
    {
        //Always move in the direction of the up vector based on the given speed
        rigidBody.velocity = speed * this.transform.up;

        //fly up
        if (directionIndex == 0)
        {
            transform.eulerAngles = new Vector3(0, 0, 0);
        }
        //fly left
        else if (directionIndex == 1)
        {
            transform.eulerAngles = new Vector3(0, 0, 90);

        }
        //fly down
        else if (directionIndex == 2)
        {
            transform.eulerAngles = new Vector3(0, 0, 90);

        }
        //fly right
        else if (directionIndex == 3)
        {
            transform.eulerAngles = new Vector3(0, 0, -90);

        }
    }

    //If off screen, reposition to fly back into camera view
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "BirdWall")
        {
            Respawn();
        }
    }

    void Respawn()
    {
        //Randomize a new index
        int newIndex = Random.Range(0, 4);
        //New index dictates new direction to fly in
        //Replace direction index to rotate appropriately
        directionIndex = newIndex;
        float newX;
            float newY;
        //Spawn somewhere along the bottom of the screen
        if (directionIndex == 0)
        {
            newX = Random.Range(bottomLeft.position.x, bottomRight.position.x);
            newY = -setY;
            this.transform.position = new Vector3(newX, newY, 0f);
        }
        //Spawn somewhere along the right of the screen
        else if (directionIndex == 1)
        {
            newY = Random.Range(bottomRight.position.y, topRight.position.y);
            newX = setX;
            this.transform.position = new Vector3(newX, newY, 0f);
        }
        //Spawn somewhere along the top of the screen
        else if (directionIndex == 2)
        {
            newX = Random.Range(topLeft.position.x, topRight.position.x);
            newY = setY;
            this.transform.position = new Vector3(newX, newY, 0f);
        }
        //Spawn somewhere along the left of the screen
        else if (directionIndex == 3)
        {
            newY = Random.Range(bottomLeft.position.y, topLeft.position.y);
            newX = -setX;
            this.transform.position = new Vector3(newX, newY, 0f);
        }

        
    }
}
