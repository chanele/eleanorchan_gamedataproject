﻿using UnityEngine;
using System.Collections.Generic;
using System.IO;
using System;
using System.Reflection;
using UnityEditor;
using System.Xml;


public class LevelManager : MonoBehaviour {

    public Transform groundHolder;
    public Transform turretHolder;
    public Transform colliderHolder;

    public enum MapDataFormat {
        Base64,
        CSV
    }

    public MapDataFormat mapDataFormat;
    public Texture2D spriteSheetTexture;

    public GameObject tilePrefab;
    public GameObject turretPrefab;
    public GameObject colliderPrefab;
    public List<Sprite> mapSprites;
    public List<GameObject> tiles;
    public List<GameObject> colliderTiles;
    public List<Vector3> turretPositions;

    Vector3 tileCenterOffset;
    Vector3 mapCenterOffset;

    public string TMXFilename;

    string gameDirectory;
    string dataDirectory;
    string mapsDirectory;
    string spriteSheetFile;
    string TMXFile;

    public int pixelsPerUnit = 32;

    public int tileWidthInPixels;
    public int tileHeightInPixels;
    float tileWidth;
    float tileHeight;

    public int spriteSheetColumns;
    public int spriteSheetRows;

    public int mapColumns;
    public int mapRows;

    public string mapDataString;
    public List<int> mapData;

    public string mapColliderDataString;
    public List<int> mapColliderData;




    // from http://answers.unity3d.com/questions/10580/editor-script-how-to-clear-the-console-output-wind.html
    static void ClearEditorConsole() {
        Assembly assembly = Assembly.GetAssembly(typeof(SceneView));
        Type type = assembly.GetType("UnityEditorInternal.LogEntries");
        MethodInfo method = type.GetMethod("Clear");
        method.Invoke(new object(), null);
    }



    static void DestroyChildren(Transform parent) {
        for (int i = parent.childCount - 1; i >= 0; i--) {
            DestroyImmediate(parent.GetChild(i).gameObject);
        }
    }



    // NOTE: CURRENTLY ONLY WORKS WITH A SINGLE TILED TILESET

    public void LoadLevel() {

        ClearEditorConsole();

        //destroy all the previous instances of tiles and turrets (prevent stacking)
        DestroyChildren(groundHolder);
        DestroyChildren(turretHolder);
        //ADDED: Destroy previous instances of collider tiles
        DestroyChildren(colliderHolder);

        // 
        {
            //Path is a class provided by System.IO
            //The Combine function connects two strings together
            //Here, Path.Combine is combining the string for the location of the Streaming Assets folder and adds 'Maps'
            //Now the string 'mapsDirectory' contains a string path for the Maps folder in the Streaming Assets folder
            mapsDirectory = Path.Combine(Application.streamingAssetsPath, "Maps");
            //Here, Path.Combine adds on to the mapsDirectory string the name of the TMX file (exact name is provided by the developer through the inspector)
            //Now, the string 'TMXFile' contains a string path to the .tmx file in the Maps folder in the STreaming Assets folder
            TMXFile = Path.Combine(mapsDirectory, TMXFilename);
        }


        // 
        {
            //mapData is a List containing integers. The .Clear function removes any elements contained in the List
            mapData.Clear();

            //ADDED: Clear collider data as well
            mapColliderData.Clear();

            //File is a class provided by System.IO
            //The ReadAllText will read through all the lines of a textfile provided at the given path
            //Here, the TMXFile string path is given to the function - The function reads through .tmx file
            //All the content read by the function is saved in a string called 'content'
            string content = File.ReadAllText(TMXFile);

            //XMLReader is provided by System.Xml
            //It uses the function Create and the class StringReader to read the XML formatted content provided by the .tmx file
            using (XmlReader reader = XmlReader.Create(new StringReader(content))) {
                
                //The function ReadToFollowing gets info from the given node in the XML file
                //Here, the XMLReader looks into the map node
                reader.ReadToFollowing("map");
                ///Convert.ToInt32 converts the given string into an integer
                //XMLReader.GetAttribute gets reference to the XML attribute named
                //Here, the XML attribute width is converted from a string to integer and is saved in the mapColumns variable
                mapColumns = Convert.ToInt32(reader.GetAttribute("width"));
                //The same is done here but with the XML attribute height and the integer mapRows
                mapRows = Convert.ToInt32(reader.GetAttribute("height"));

                //The XMLReader looks into the tileset node
                //From here on, the reader cannot access any attributes in the map node
                reader.ReadToFollowing("tileset");

                //The XML attribute tilewidth is converted into an integer and saved in tileWidthInPixels
                tileWidthInPixels = Convert.ToInt32(reader.GetAttribute("tilewidth"));
                //The XML attribute tileheight is converted into an integer and saved in tileHeightInPixels
                tileHeightInPixels = Convert.ToInt32(reader.GetAttribute("tileheight"));

                //The XML attribute tilecount is converted into an integer and saved in spriteSheetTileCount
                int spriteSheetTileCount = Convert.ToInt32(reader.GetAttribute("tilecount"));
                //The XML attribute columns is converted into an integer and saved in spriteSheetColumns
                spriteSheetColumns = Convert.ToInt32(reader.GetAttribute("columns"));

                //The value for the sprite sheet tile count is divided by the number of the columns given by the .tmx file
                //The result is the number of rows in the sprite sheet, and is saved in the appropriately named variable
                spriteSheetRows = spriteSheetTileCount / spriteSheetColumns;

                //The XMLReader looks into the image node
                //Can no longer access the tileset node
                reader.ReadToFollowing("image");
                //File path for the sprite sheet is saved in a string created by combining the string path to the maps folder and the name of the file provided by getting the string contained in the attribute source
                spriteSheetFile = Path.Combine(mapsDirectory, reader.GetAttribute("source"));

                //The XMLReader looks into the layer node
                reader.ReadToFollowing("layer");

                //The XMLReader looks into the data node within the layer node
                reader.ReadToFollowing("data");

                //The XML attribute encoding is saved in the string encodingType
                string encodingType = reader.GetAttribute("encoding");

                //Depending on the encoding type given, change the map data accordingly
                switch (encodingType) {
                    //if the map encoding is base64, change the format accordingly
                    case "base64":
                        mapDataFormat = MapDataFormat.Base64;
                        break;
                    //if the map encoding is csv, change the format accordingly
                    case "csv":
                        mapDataFormat = MapDataFormat.CSV;
                        break;
                }

                //ReadElementContentAsString looks at the contents of the last opened node (in this case, the data node) and returns it as a string
                //Adding the Trim function removes any blank spaces in the string created
                mapDataString = reader.ReadElementContentAsString().Trim();

                //ADDED: Read the next layer node
                reader.ReadToNextSibling("layer");
                //Read the data node within
                reader.ReadToFollowing("data");
                //Read content within and return as a string
                //Encoding has already been retrieved from the last data node, do not need to do it again
                mapColliderDataString = reader.ReadElementContentAsString().Trim();

                //turretPositions is a List containing Vector3 positions for the turrets. The .Clear function removes any elements contained in the List
                turretPositions.Clear();

                //The following finds the turret positions contained in the .tmx file and saves the positions in a list

                //if the XMLReader can find and read the node objectgroup...
                if (reader.ReadToFollowing("objectgroup")) {
                    //ReadToDescendent checks for a node within the selected node
                    //In this case, checking for an object node inside the node objectgroup
                    //If the XMLReader goes down to the next element and it has the given name...
                    if (reader.ReadToDescendant("object")) {
                        //do-while loop executes the statements in the lop before checking the condition
                        do {
                            //The XML attribute x is converted into a float and divided by the value in the variable pixelsPerUnit and saved in x
                            float x = Convert.ToSingle(reader.GetAttribute("x")) / (float)pixelsPerUnit;
                            //The XML attribute x is converted into a float and divided by the value in the variable pixelsPerUnit and saved in y
                            float y = Convert.ToSingle(reader.GetAttribute("y")) / (float)pixelsPerUnit;
                            //A new vector3 is created with the x and y values created and added to the turretPositionsList
                            turretPositions.Add(new Vector3(x, -y, 0));
                            //ReadToNextSibling checks for a node next to the currently selected node (not nested within it)
                            //Here, it is checking for another object node at the same level as the previous node
                            //This will loop if the XMLReader goes to the next element and it has the given name
                        } while (reader.ReadToNextSibling("object"));
                    }
                }

            }


            switch (mapDataFormat) {
                //if the map data format is base64...
                case MapDataFormat.Base64:
                    //FromBase64String converts the string of map data from the data node into an array of bytes
                    byte[] bytes = Convert.FromBase64String(mapDataString);
                    int index = 0;
                    //Go through the whole array of bytes...
                    while (index < bytes.Length) {
                        //Create a tileID by converting 4 bytes in the given array at the position provided by the index into an integer
                        int tileID = BitConverter.ToInt32(bytes, index) - 1;
                        //add the tileID to a List of integers containing the map data
                        mapData.Add(tileID);
                        //Move the index up by 4 since 4 bytes are used starting from the index starting point
                        index += 4;
                    }

                    //Repeat the above for collider map data
                    byte[] colliderBytes = Convert.FromBase64String(mapColliderDataString);
                    int colliderIndex = 0;
                    while(colliderIndex < bytes.Length)
                    {
                        int tileID = BitConverter.ToInt32(colliderBytes, colliderIndex) - 1;
                        mapColliderData.Add(tileID);
                        colliderIndex += 4;
                    }
                    break;

                    //if the map data format is csv...
                case MapDataFormat.CSV:

                    //split the string of map data into substrings, split where the given characters in the separator array are (in this case a blank space)
                    //save substrings in a string array
                    string[] lines = mapDataString.Split(new string[] { " " }, StringSplitOptions.None);
                    //for each string in the array...
                    foreach (string line in lines) {
                        //separate the string into substrings where the comma is
                        //save substrings in a string array
                        string[] values = line.Split(new string[] { "," }, StringSplitOptions.None);
                        //for each string in the values array
                        foreach (string value in values) {
                            //convert the string into an integer and save as the tileID
                            int tileID = Convert.ToInt32(value) - 1;
                            //add the tileID to a List of integers containing the map data
                            mapData.Add(tileID);
                        }
                    }
                    break;

            }

        }


        {
            //Divide the tile width and height from the .tmx files by the given pixelsPerUnit to find the tile width and height to use
            tileWidth = (tileWidthInPixels / (float)pixelsPerUnit);
            tileHeight = (tileHeightInPixels / (float)pixelsPerUnit);

            //Adjust the center of the tiles based on the tile width and height
            tileCenterOffset = new Vector3(0.5f * tileWidth, -.5f * tileHeight, 0);
            //Adjust the center of the map based on the number of columns and rows multiplied by the tile dimensions
            mapCenterOffset = new Vector3(-(mapColumns * tileWidth) * .5f, (mapRows * tileHeight) * .5f, 0);

        }




        // 
        {
            //create a new texture2D with the given width and height
            spriteSheetTexture = new Texture2D(2, 2);
            //Use ReadAllBytes on the sprite sheet file to read and place bytes in an array
            //LoadImage loads the byte array to load the png sprite sheet
            spriteSheetTexture.LoadImage(File.ReadAllBytes(spriteSheetFile));
            //change the texture filter to point filter, pixels will become blocky looking when the viewer is close
            spriteSheetTexture.filterMode = FilterMode.Point;
            //Change the wrap mode to clamp so the texture does not repeat
            spriteSheetTexture.wrapMode = TextureWrapMode.Clamp;
        }


        // 
        {
            //Remove any sprites in the List of map sprites
            mapSprites.Clear();

            //for each tile in the sprite sheet...
            for (int y = spriteSheetRows - 1; y >= 0; y--) {
                for (int x = 0; x < spriteSheetColumns; x++) {
                    //create a new sprite:
                    //use the sprite sheet texture
                    //create a rect using the tile dimensions in pixels at the given position
                    //pivot point positioned at the center of the sprite (0.5f, 0.5f)
                    Sprite newSprite = Sprite.Create(spriteSheetTexture, new Rect(x * tileWidthInPixels, y * tileHeightInPixels, tileWidthInPixels, tileHeightInPixels), new Vector2(0.5f, 0.5f), pixelsPerUnit);
                    //add new sprite to the List of map sprites
                    mapSprites.Add(newSprite);
                }
            }
        }

        // 
        {
            //Remove any gameobjects in the List of tiles
            tiles.Clear();
            //For each tile in the map...
            for (int y = 0; y < mapRows; y++) {
                for (int x = 0; x < mapColumns; x++) {

                    //Find the appropriate index for the map data List
                    int mapDatatIndex = x + (y * mapColumns);
                    //Get the tileID from the map data List using the index (know which section of texture to load in at that position)
                    int tileID = mapData[mapDatatIndex];

                    //Instantiate and position a tile gameobject using the given tile prefab at the appropriate location based on the tile dimensions, current position in the rows and columns, and the center offsets
                    GameObject tile = Instantiate(tilePrefab, new Vector3(x * tileWidth, -y * tileHeight, 0) + mapCenterOffset + tileCenterOffset, Quaternion.identity) as GameObject;
                    //Get reference to spriterenderer in the child
                    //Select the correct section from the map sprites List using the tileID and set it in the renderer
                    tile.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = mapSprites[tileID];
                    //Make the tile gameobject a child under the groundholder gameobject
                    tile.transform.parent = groundHolder;
                    //Add the new tile to the tiles List
                    tiles.Add(tile);
                }
            }
        }

        {
            //ADDED: Remove gameobjects in the list of collider tiles
            colliderTiles.Clear();
            //For each tile in the map
            for (int y = 0; y < mapRows; y++)
            {
                for (int x = 0; x < mapColumns; x++)
                {
                    //Find appropriate index for the map data list
                    int mapDatatIndex = x + (y * mapColumns);
                    //Get the tileID from the collider map data list using the index
                    int tileID = mapColliderData[mapDatatIndex];
                    //For nonexistent tiles, dont instantiate anything
                    if (tileID == -1f)
                    {
                       
                    }
                    else
                    {
                        //Instantiate and position a tile game object using the collider prefab
                        GameObject tile = Instantiate(colliderPrefab, new Vector3(x * tileWidth, -y * tileHeight, 0) + mapCenterOffset + tileCenterOffset, Quaternion.identity) as GameObject;
                        //Find the spriterenderer in the child and assite the appropriate sprite
                        tile.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = mapSprites[tileID];
                        //Find and adjust collider size to tile size (if different)
                        tile.transform.GetChild(0).GetComponent<BoxCollider2D>().size = new Vector2(tileWidth, tileHeight);
                        //Parent to the holder
                        tile.transform.parent = colliderHolder;
                        //Add to the list
                        colliderTiles.Add(tile);
                    }
                }
            }
        }


        // 
        {
            //for each turrent position saved in the List...
            foreach (Vector3 turretPosition in turretPositions) {
                //Instantiate and position a turret gameobject using the given turret prefab, the given position from the List, and the map center offset
                GameObject turret = Instantiate(turretPrefab, turretPosition + mapCenterOffset, Quaternion.identity) as GameObject;
                //rename the gameobject to Turret
                turret.name = "Turret";
                //make this turrent a child under the turrentholder gameobject
                turret.transform.parent = turretHolder;
            }
        }

       //get the current date and time from the computer
        DateTime localDate = DateTime.Now;
        //print the hour, minute, and second the level was loaded
        print("Level loaded at: " + localDate.Hour + ":" + localDate.Minute + ":" + localDate.Second);
    }
}


